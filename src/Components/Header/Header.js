import '../Header/Header.css'
import logo from './img/logo.svg'

const Header = () => {
    return (
        <div>
            <header className="header">
                <img className="header-logo" src={ logo } alt=""/>
                <nav>
                    <ul className="nav">
                        <li className="nav-item"><a className="nav-link" href="">Главная</a></li>
                        <li className="nav-item"><a className="nav-link" href="">График</a></li>
                        <li className="nav-item"><a className="nav-link" href="">О нас</a></li>   
                    </ul>
                </nav>
        </header>
        </div>
    )
}

export default Header;