import '../Footer/Footer.css';

import rinx from './img/rinx.svg';
import rnd from './img/rnd.svg'
import vk from './img/vk.svg'
import git from './img/git.svg'


const Footer = () => {
    return (
        <div className="wrapper">
            <footer>
                <div className="footer logo">
                    <img src={ rinx } alt="" />
                    <img src={ rnd } alt="" />
                </div>
                <p className="footer-copyright">Региональный хакатон «Кибербезопасность граждан» 2022г.</p>
                    <div className="footer-icon">
                        <img src={ vk } alt="" />
                        <img src={ git } alt="" />
                    </div>
            </footer>
        </div>
    )
}

export default Footer;