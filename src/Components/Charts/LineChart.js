import React, {useState, useEffect} from 'react'
import {Chart as ChartJS, LineElement, PointElement,CategoryScale, LinearScale} from 'chart.js'
import {Line} from 'react-chartjs-2'

ChartJS.register(
    CategoryScale,
    LinearScale,
    LineElement,
    PointElement
)

const LineChart = () => {

    const [chart, setChart] = useState([])

    let baseUrl = "https://yfapi.net/v6/finance/quote?region=US&lang=en&symbols=AAPL%2CBTC-USD%2CEURUSD%3DX"
    // let proxyUrl = "https://cors-anywhere.herokuapp.com/"
    let apiKey = "C4Ja0gRoXF27e88GnY4bs9PMFgrxfTrZ5jitUrXj"

    useEffect(() => {
        const fetchCoins = async () => {
            await fetch(`${baseUrl}`, {
                method: 'GET',
                headers: {
                    // 'Content-Type' : 'application/json',
                    'x-api-key' : `${apiKey}`
                }
            }).then((response) => {
                response.json().then((json) => {
                    console.log(json)
                    setChart(json.quoteResponse)
                })
            }).catch(error => {
                console.log(error);
            })
        }
        fetchCoins()
    }, [baseUrl, apiKey])
    
    console.log("chart", chart)
    // console.log(chart?.price)
    
    let data = {
        labels: chart?.result?.map(x => x.longName),
        datasets: [{
            label: `${chart?.result?.length} Имеется акций`,
            data: chart?.result?.map(x => x.ask),
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    }

    let options = {
        maintainAspectRadio: false,
        scales: {
            y: {
                beginAtZero: true
            }
        },
        legend: {
            labels: {
                fontSize: 26
            }
        }
    }

  return (
    <div>
        <Line 
            data = {data}
            height = {100} 
            options = {options}
        />
    </div>
  )
}

export default LineChart;