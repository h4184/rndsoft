import Footer from './Components/Footer/Footer';
import Header from './Components/Header/Header';
import Main from './Components/Main/Main';

function App() {
  return (
    <div className="App">
      <div className='wrapper'>
        <Header />
        <Main />
        <Footer />
      </div>
    </div>
  );
}

export default App;